package cn.test;

import org.junit.Test;

import com.code.QRCodeEncoder;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2018/3/12
 */
public class QrcodeTest {

    @Test
    public void generateQrcodeWithLogo(){
        int size = 16;
        String imgPath = "d:/test/qrcode/qr_logo"+size+".png";
        String logo = "d:/test/qrcode/logo.jpg";
        String content = "http://so.com";
        QRCodeEncoder encoder = new QRCodeEncoder();
        encoder.encoderQRCode(
                content, //内容
                imgPath, //文件路径
                "png",   //文件类型
                "UTF-8", //编码方式
                size,      //大小
                null,   //边框
                null, //前景色
                null, //背景色
                4,    //图标比例
                logo, //图标路径
                null  //动画图标路径
        );
    }

}
